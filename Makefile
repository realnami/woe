INCLUDE=-I /usr/local/include/quickjs -I ../../js/quickjs/
CFLAGS=$(INCLUDE) -fPIC -DJS_SHARED_LIBRARY

RELEASE_FLAGS=-flto # use link time optimization
RELEASE_FLAGS+=-fno-date
RELEASE_FLAGS+=-fno-eval
RELEASE_FLAGS+=-fno-string-normalize
RELEASE_FLAGS+=-fno-regexp
RELEASE_FLAGS+=-fno-json
RELEASE_FLAGS+=-fno-proxy
RELEASE_FLAGS+=-fno-map
RELEASE_FLAGS+=-fno-typedarray
RELEASE_FLAGS+=-fno-promise
#RELEASE_FLAGS+=-fno-module-loader
RELEASE_FLAGS+=-fno-bigint


JS_CC=../../js/quickjs/qjsc
LIBQJS=../../js/quickjs/libquickjs.a

DEPENDECY=file_storage.js woe_menu.js file_search.js


.PHONY: release


all: woe.app

release: woe.release.app


woe.app: main.c vt100.c woe.js $(DEPENDECY)
	$(CC) -o $@ $< $(INCLUDE) $(LIBQJS) -lm vt100.c

main.c: woe.js
	$(JS_CC) -e -o $@ $< -M VT100,vt100

woe.release.app: main.release.c vt100.c woe.js $(DEPENDECY)
	$(CC) -o $@ $< $(INCLUDE) $(LIBQJS) -lm vt100.c

main.release.c: woe.js
	$(JS_CC) -e -o $@ $< $(RELEASE_FLAGS) -M VT100,vt100


.PHONY: clean test
CLEAN_FILES=woe.app main.c woe.release.app main.release.c
clean:
	-rm $(CLEAN_FILES)


#include distro/slackware