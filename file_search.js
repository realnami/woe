export var file_search = {
    points: [],
    index: 0,
};


file_search.reset = function () {
    this.points = [];
    this.index = 0;
};


file_search.run = function (terminal) {
    var query = terminal.prompt("search: %s");

    if (!query) {
        return;
    }

    let i;
    for (i = 0; i < terminal.numrows; i++) {
        let v = terminal.search_at(query, 0, i);

        while (v == 0 || v) {
            this.points.push([v, i + 1]);
            v++;
            v = terminal.search_at(query, v, i);
        }
    }

    if (this.points.length > 0) {
        let [cx, cy] = this.points[0];

        terminal.move_to_line(cy);
        terminal.cx = cx;
    }
}


file_search.next = function (terminal) {
    if (this.points.length == 0) {
        return;
    }

    this.index++;

    if (this.index >= this.points.length) {
        this.index = 0;
    }

    let [cx, cy] = this.points[this.index];

    terminal.move_to_line(cy);
    terminal.cx = cx;
}


file_search.prev = function (terminal) {
    if (this.points.length == 0) {
        return;
    }

    this.index--;

    if (this.index <= -1) {
        this.index = this.points.length - 1;
    }

    let [cx, cy] = this.points[this.index];

    terminal.move_to_line(cy);
    terminal.cx = cx;
}
